package com.abonda;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;

/**
 * This class is used to define to create all the users in the application.
 * 
 * @author vchukka
 */
public class AllUsers {

	private HashSet<User> listOfAllUsers;
	//private HashSet<User> AllUsers;
	private HashSet<User> MinAgeList;

	// default constructor
	public AllUsers() {
		listOfAllUsers = new HashSet<User>();
		//AllUsers = new HashSet<User>();
	}

	/**
	 * This is the helper method to create newUsers.
	 * 
	 * @vchukka name
	 * @vchukka age
	 * @vchukka profession
	 * @vchukka primaryhobbie
	 * @vchukka gender
	 * @vchukka ph_no
	 * @vchukka martial_Status
	 * @vchukka location
	 * @vchukka salary
	 * @vchukka Height
	 * @vchukka Weight
	 * @vchukka Edu_qulification
	 * @vchukka foods
	 * @vchukka fav_places
	 * @vchukka hobbies
	 * @return
	 * @throws Exception
	 */
	public boolean createnewBlunderUser(String name, int age, String profession, String primaryhobbie, String Gender,
			String ph_no, String martial_Status, String location, int salary, int Height, int Weight,
			String Edu_qualification[], String fav_foods[], String fav_places[], String[] hobbies) throws Exception {

		User myBlunderuser = new User(name, age, profession, primaryhobbie, Gender, ph_no, martial_Status, location,
				salary, Height, Weight, Edu_qualification, fav_foods, fav_places, hobbies);
		this.listOfAllUsers.add(myBlunderuser);

		return false;
	}

	/**
	 * This is a helper method, to print all Users in collection in human readable
	 * format.
	 * 
	 * @return
	 */
	public void printAllUsers() {
		for (User curruser : listOfAllUsers) {
			System.out.println(curruser);
		}

	}

	/**
	 * This is the helper method to search the strings and get the user details
	 * 
	 * @return
	 * 
	 * @vchukka searchString
	 */
	public HashSet<User> returnSearchResultsUsers(String searchString) {
		HashSet<User> returnSet = new HashSet<User>();

		for (User currUser : listOfAllUsers) {
			if (currUser.getName().contains(searchString) || currUser.getProfession().contains(searchString)
					|| currUser.getPrimaryhobbie().contains(searchString) || currUser.getGender().contains(searchString)
					|| currUser.getLocation().contains(searchString)
					|| currUser.getEdu_qualification().contains(searchString)
					|| currUser.getFoods().contains(searchString) || currUser.getFav_places().contains(searchString)
					|| currUser.getHobbies().contains(searchString)) {
				returnSet.add(currUser);
				System.out.println(currUser.getName());
				System.out.println(currUser.getProfession());
				System.out.println(currUser.getPrimaryhobbie());
				System.out.println(currUser.getGender());
				System.out.println(currUser.getLocation());
				System.out.println(currUser.getEdu_qualification());
				System.out.println(currUser.getFoods());
				System.out.println(currUser.getFav_places());
				System.out.println(currUser.getHobbies());
			}

		}
		return returnSet;
	}

	/**
	 * This is helper method to find a userdetails by its name
	 * 
	 * @vchukka state
	 * @return the user object if found, or null object if not found
	 */
	public User getMatchUserByName(String username) {
		for (User currUser : listOfAllUsers) {
			if (currUser.getPrimaryhobbie().matches(username)) {

				return currUser;
			}
		}
		return null;
	}

	/**
	 * This is helper method to find a userdetails by its location
	 * 
	 * @param location
	 * @return
	 */
	public User findMathByLocation(String location) {
		for (User curruser : listOfAllUsers) {
			if (curruser.getLocation().matches(location)) {
				return curruser;
			}

		}
		return null;
	}

	/**
	 * This is the helper method to find a userdetails by its gender
	 * 
	 * @param Gender
	 * @return
	 */
	public User findMatchByGender(String Gender) {
		for (User curruser : listOfAllUsers) {
			if (curruser.getGender().matches(Gender)) {
				return curruser;
			}

		}
		return null;

	}

	/**
	 * This is the helper method to find a userdetails by its martial_Status
	 * 
	 * @param martial_Status
	 * @return
	 */
	public User findMatchBymartialStatus(String martial_Status) {
		for (User curruser : listOfAllUsers) {
			if (curruser.getMartial_Status().matches(martial_Status)) {
				return curruser;
			}

		}
		return null;
	}

	/**
	 * This is the helper method find a userdetails by its profession
	 * 
	 * @param profession
	 * @return
	 */
	public User findByProfession(String profession) {
		for (User curruser : listOfAllUsers) {
			if (curruser.getProfession().matches(profession)) {
				return curruser;
			}

		}
		return null;
	}

	/**
	 * This is the helper method find a userdetails by its Qualification
	 * 
	 * @param Edu_qualification
	 * @return
	 */
	public User findByEduQualification(String Edu_qualification) {
		for (User curruser : listOfAllUsers) {
			if (curruser.getEdu_qualification().matches(Edu_qualification)) {
				return curruser;
			}

		}
		return null;
	}

	/**
	 * This is the helper method find a userdetails by its favfoods
	 * 
	 * @param fav_foods
	 * @return
	 */
	public User findByfavFoods(String fav_foods) {
		for (User curruser : listOfAllUsers) {
			if (curruser.getFoods().matches(fav_foods)) {
				return curruser;
			}

		}
		return null;

	}

	public ArrayList<User> findMatchByFoodsAnd(String foods[]) {
		ArrayList<User> getFoodAndlist = new ArrayList<User>();
		User newuser = null;
		String food1 = foods[0];
		String food2 = foods[1];
		for (User currentuser : this.listOfAllUsers) {
			if ((Arrays.asList(currentuser.getFoods()).contains(food1)
					&& (Arrays.asList(currentuser.getFoods()).contains(food2)))) {
				newuser = currentuser;
				getFoodAndlist.add(newuser);
			}
		}
		return getFoodAndlist;
	}

	/**
	 * This is helper method to find a userdetails by search favPlaces
	 * 
	 * @param fav_palces
	 * @return
	 */
	public User findByfavPlaces(String fav_palces) {
		for (User curruser : listOfAllUsers) {
			if (curruser.getFav_places().matches(fav_palces)) {
				return curruser;
			}

		}
		return null;
	}

	/**
	 * This is the helper method to find a userdetails by search Hobbies
	 * 
	 * @param hobbies
	 * @return
	 */
	public User findByHobbies(String hobbies) {
		for (User curruser : listOfAllUsers) {
			if (curruser.getHobbies().matches(hobbies)) {
				return curruser;
			}

		}
		return null;
	}

	/**
	 * This is the helper method to find a user details by its
	 * 
	 * @param searchint
	 * @return
	 */

//	public HashSet<User> returnSearchResultsUser(int searchint) {
//		HashSet<User> result = new HashSet<User>();
//
//		for (User currentuser : listOfAllUsers) {
//			if (currentuser.getAge() == (searchint) || currentuser.getHeight() == (searchint)
//					|| currentuser.getSalary() == (searchint)) {
//				result.add(currentuser);
//				System.out.println(currentuser.getAge());
//				System.out.println(currentuser.getHeight());
//				System.out.println(currentuser.getSalary());
//				System.out.println(currentuser.getWeight());
//
//			}
//
//		}
//
//		return result;
//	}
//
//	/**
//	 * This is helper method to find a user by its age
//	 * 
//	 * @param age
//	 * @return //
//	 */
//	public User findMatchByage(int age) {
//		for (User curruser : AllUsers) {
//			if (curruser.getAge() == age) {
//				return curruser;
//			}
//
//		}
//		return null;
//
//	}
//
//	/**
//	 * This is helper method to find a user by its Minimum age
//	 * 
//	 * @param MinAge
//	 * @return
//	 */
//	public User findMatchByMinAge(int MinAge) {
//		for (User curruser : AllUsers) {
//			if (curruser.getAge() < MinAge) {
//				return curruser;
//			}
//
//		}
//		return null;
//
//	}
//
//	/**
//	 * This is helper method to find a user by its Maximum age
//	 * 
//	 * @param MaxAge
//	 * @return
//	 */
//	public User findMatchByMaxAge(int MaxAge) {
//		for (User curruser : AllUsers) {
//			if (curruser.getAge() > MaxAge) {
//				return curruser;
//			}
//
//		}
//		return null;
//	}
//
//	/**
//	 * This is helper method to find a user by its Height
//	 * 
//	 * @param Height
//	 * @return
//	 */
//	public User findMatchByHeight(int Height) {
//		for (User curruser : AllUsers) {
//			if (curruser.getHeight() == Height) {
//
//				return curruser;
//			}
//
//		}
//		return null;
//	}
//
//	/**
//	 * This is helper method to find a user by its Maxmum Height
//	 * 
//	 * @param MaxHeight
//	 * @return
//	 */
//	public User findMatchByMaxHeight(int MaxHeight) {
//		for (User curruser : AllUsers) {
//			if (curruser.getHeight() > MaxHeight) {
//				return curruser;
//
//			}
//
//		}
//		return null;
//
//	}
//
//	/**
//	 * This is the helper method to find a user by its Minimum Height
//	 * 
//	 * @param MinHeight
//	 * @return
//	 */
//	public User findMatchByMinHeight(int MinHeight) {
//		for (User curruser : AllUsers) {
//			if (curruser.getHeight() < MinHeight) {
//				return curruser;
//			}
//
//		}
//		return null;
//	}
//
//	/**
//	 * This is helper method to find a user by its Weight
//	 * 
//	 * @param Weight
//	 * @return
//	 */
//	public User findMatchByWeight(int Weight) {
//		for (User curruser : AllUsers) {
//			if (curruser.getWeight() == Weight) {
//				return curruser;
//			}
//
//		}
//		return null;
//
//	}
//
//	/**
//	 * This is the helper method to find a user by its Minimum Weight
//	 * 
//	 * @param MinWeight
//	 * @return
//	 */
//	public User findMatchByMinWeight(int MinWeight) {
//		for (User curruser : AllUsers) {
//			if (curruser.getWeight() < MinWeight) {
//				return curruser;
//			}
//
//		}
//		return null;
//	}
//
//	/**
//	 * This is helper method to find a user by its Max Weight
//	 * 
//	 * @param MaxWeight
//	 * @return
//	 */
//	public User findMatchByMaxWeight(int MaxWeight) {
//		for (User curruser : AllUsers) {
//			if (curruser.getWeight() > MaxWeight) {
//				return curruser;
//			}
//		}
//		return null;
//
//	}
//
//	/**
//	 * This is helper method to find a user by its salary
//	 * 
//	 * @param salary
//	 * @return
//	 */
//	public User findMatchBysalary(int salary) {
//		for (User curruser : AllUsers) {
//			if (curruser.getSalary() == salary) {
//				return curruser;
//
//			}
//
//		}
//		return null;
//	}
//
//	/**
//	 * This is helper method to find a user by its Minimum Salary
//	 * 
//	 * @param MinSalary
//	 * @return
//	 */
//	public User findMatchByMinSalary(int MinSalary) {
//		for (User curruser : AllUsers) {
//			if (curruser.getSalary() < MinSalary) {
//				return curruser;
//			}
//
//		}
//		return null;
//	}
//
//	/**
//	 * This is the helper method to find a user by its Maximum Salary
//	 * 
//	 * @param MaxSalary
//	 * @return
//	 */
//	public User findMatchByMaxSalary(int MaxSalary) {
//		for (User curruser : AllUsers) {
//			if (curruser.getSalary() > MaxSalary) {
//				return curruser;
//			}
//
//		}
//		return null;
//	}
	/**
	 * This is a helper method to find user by its minimun Height
	 * @param Min_height
	 * @return
	 */
public ArrayList<User> findMatchByMinHeight(int Min_height){
	ArrayList<User> MinHeightList = new ArrayList<User>();
	System.out.println("Minimum height is :\n");
	for (User curruser : listOfAllUsers) {
		if(curruser.Height < Min_height) {
			MinHeightList.add(curruser);
			
		}
		
	}
	return MinHeightList;
}
/**
 * This is a helper method to find user by its Maximum Height 
 * @param Max_Height
 * @return
 */
public ArrayList<User> findMatchByMaxHeight(int Max_Height){
	ArrayList<User> Max_heightList = new ArrayList<User>();
	System.out.println("max_height is:\n");
	//System.out.println("Maximum height are :\n");
	for (User curruser : listOfAllUsers) {
		if(curruser.Height > Max_Height) {
			Max_heightList.add(curruser);
			
		}
		
	}
	return Max_heightList;
}
/**
 * This is helper method to find user by its Height between Min height and Max height 
 * @param Min_height
 * @param Max_height
 * @return
 */
public ArrayList<User> findMatchByBetweenHeight(int Min_height,int Max_height){
	ArrayList<User> BetweenheightList = new ArrayList<User>();
	System.out.println("Between  heights are :\n");
	for (User curruser : listOfAllUsers) {
		if(curruser.Height>Min_height && curruser.Height<Max_height) {
			BetweenheightList.add(curruser);
			
		
		}
		
	}
	return BetweenheightList;
}
/**
 * This is helper method to find user by its Minimum Weight 
 * @param Min_Weight
 * @return
 */
public ArrayList<User> findMatchByMinWeight(int Min_Weight){
	ArrayList<User> MinWeightList = new ArrayList<User>();
	//System.out.println("Minimum height are :\n");
	for (User curruser : listOfAllUsers) {
		if(curruser.Weight<Min_Weight) {
			MinWeightList.add(curruser);
			
		}
		
	}
	return MinWeightList;
}
/**
 * This is helper method to find user by its Maximum Weight 
 * @param Max_Weight
 * @return
 */
public ArrayList<User> findMatchByMax_Weight(int Max_Weight){
	ArrayList<User> MaxWeightList = new ArrayList<User>();
	System.out.println("Maximum weight are :\n");
	for (User curruser : listOfAllUsers) {
		if(curruser.Weight<Max_Weight) {
			MaxWeightList.add(curruser);
			
		}
		
	}
	return MaxWeightList;
}
/**
 * This is helper method to find user by its Weight between Min Weight and Max Weight 
 * @param Min_Weight
 * @param Max_Weight
 * @return
 */
public ArrayList<User> findMatchByBetweenWeight(int Min_Weight,int Max_Weight){
	ArrayList<User> BetweenWeightList = new ArrayList<User>();
	System.out.println("Between  Weights are :\n");
	for (User curruser : listOfAllUsers) {
		if(curruser.Weight>Min_Weight && curruser.Weight<Max_Weight) {
			BetweenWeightList.add(curruser);
			
		
		}
		
	}
	return BetweenWeightList;
}
/**
 * This is helper method to find user by its Minimum Age
 * @param Min_Age
 * @return
 */
public ArrayList<User> findMatchByMinAge(int Min_Age){
	ArrayList<User> MinAgeList = new ArrayList<User>();
	//System.out.println("Minimum height are :\n");
	for (User curruser : listOfAllUsers) {
		if(curruser.age<Min_Age) {
			MinAgeList.add(curruser);
			
		}
		
	}
	return MinAgeList;
}
/**
 * This is helper method to find user by its Maximum Age
 * @param Max_Age
 * @return
 */
public ArrayList<User> findMatchByMax_Age(int Max_Age){
	ArrayList<User> MaxAgeList = new ArrayList<User>();
	System.out.println("Maximum ages are :\n");
	for (User curruser : listOfAllUsers) {
		if(curruser.age<Max_Age) {
			MaxAgeList.add(curruser);
			
		}
		
	}
	return MaxAgeList;
}
/**
 * This is helper method to find user by Age Between Min age and Max age 
 * @param Min_Age
 * @param Max_Age
 * @return
 */
public ArrayList<User> findMatchByBetweenAge(int Min_Age,int Max_Age){
	ArrayList<User> BetweenAgeList = new ArrayList<User>();
	System.out.println("Between  Ages are :\n");
	for (User curruser : listOfAllUsers) {
		if(curruser.age>Min_Age && curruser.age<Max_Age) {
			BetweenAgeList.add(curruser);
			
		
		}
		
	}
	return BetweenAgeList;
}
/**
 * This is helper method to find user by Minimum Salary
 * @param Min_Salary
 * @return
 */
public ArrayList<User> findMatchByMinSalary(int Min_Salary){
	ArrayList<User> MinSalaryList = new ArrayList<User>();
	//System.out.println("Minimum height are :\n");
	for (User curruser : listOfAllUsers) {
		if(curruser.salary<Min_Salary) {
			MinSalaryList.add(curruser);
			
		}
		
	}
	return MinSalaryList;
}
/**
 * This is helper method to find user by Maximum Salary
 * @param Max_Salary
 * @return
 */
public ArrayList<User> findMatchByMax_Salary(int Max_Salary){
	ArrayList<User> Max_SalaryList = new ArrayList<User>();
	//System.out.println("Minimum height are :\n");
	for (User curruser : listOfAllUsers) {
		if(curruser.salary<Max_Salary) {
			Max_SalaryList.add(curruser);
}
	}
	return Max_SalaryList;
}
/**
 * This is helper method to find user by Salary Between Min Salary and Max Salary
 * @param Min_Salary
 * @param Max_Salary
 * @return
 */
public ArrayList<User> findMatchByBetweenSalary(int Min_Salary,int Max_Salary){
	ArrayList<User> BetweenSalaryList = new ArrayList<User>();
	System.out.println("Between  Salary are :\n");
	for (User curruser : listOfAllUsers) {
		if(curruser.salary>Min_Salary && curruser.salary<Max_Salary) {
			BetweenSalaryList.add(curruser);
			
		
		}
		
	}
	return BetweenSalaryList;
}
}
	
