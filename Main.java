package com.abonda;

import java.util.ArrayList;


import java.util.HashSet;

/**
 * This the Main class it contains a main method to test various features during
 * development of an application
 * 
 * @author vchukka
 */
public class Main {
	ArrayList<User> listofUsers;

	

	
	/**
	 * This is the main method to test the application.
	 * 
	 * @vchukka args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception  {
		Main mainobject = new Main();
		mainobject.testUserCreation();
	}

	/**
	 * This is the helper method to test user creation.
	 * @param listOfAllUsers 
	 * @throws Exception 
	 */
	private void testUserCreation() throws Exception {
		// New user object creation using blunder user collection
		

			AllUsers bc = new AllUsers();
			bc.createnewBlunderUser("krishna", 23, "java developer", "playing", "Male", "8790312879", "single",
					"Gn palem", 20000, 175, 67, new String[] { "10th", "diploma", "B_tech" },
					new String[] { "Biriyani", "manchurian", "kfc" }, new String[] { "Araku", "Tirupathi", "gujarat" },
					new String[] { "palying", "eating", "sleeping" });

			bc.createnewBlunderUser("Rekha", 21, "java developer", "sleeping", "female", "9957638474", "single",
					"warangal", 20000, 150, 45, new String[] { "10th", "inter", "B_tech" },
					new String[] { "panipuri", "noodles", "ice-cream" }, new String[] { "hyd", "vizag", "delhi" },
					new String[] { "sleeping", "crying", "caring" });

			bc.createnewBlunderUser("Bala", 25, "Indian Army", "playing", "male", "8886008205", "Married", "Bapatla",
					52000, 155, 72, new String[] { "10th", "inter", "Degree" },
					new String[] { "pizza", "pulav", "fried rice" }, new String[] { "Araku", "pondicherry", "ooty" },
					new String[] { "tavelling", "running", "thinking creatively" });

			bc.createnewBlunderUser("priya", 20, "java BDA developer", "making friends", "female", "7585947485",
					"single", "warangal", 22000, 200, 42, new String[] { "10th", "diploma", "B_tech" },
					new String[] { "andhra meals", "Biriyani", "kfc" }, new String[] { "Italy", "France", "Mumbai" },
					new String[] { "caring", "making noise", "making frnds" });

			bc.createnewBlunderUser("phani", 21, "Indian CRPF", "Eating", "Male", "7036479655", "comitted", "Gn palem",
					25000, 250, 77, new String[] { "10th", "inter", "Degree" },
					new String[] { "Mandhi", "Pizza", "Biriyani" }, new String[] { "vizag", "vijayawada", "Banglore" },
					new String[] { "caring", "sleeping", "easily trust anyone" });
			
//		 HashSet<User> results=bc.returnSearchResultsUsers("krishna");
//			System.out.println(results.size());
//		 HashSet<User> result =bc.returnSearchResultsUser(23);
//		 	System.out.println(result.size());	
//		 	ArrayList<User> hobbiesAnd = bc.findMatchByFoodsAnd(new String[] { "Dancing", "playing" });
//			System.out.println(hobbiesAnd);
//			listofUsers=bc.findMatchByMinHeight(200);
//			for (User iterable : listofUsers) {
//				System.out.println(iterable);
//				
			//}
			listofUsers=bc.findMatchByMinHeight(130);
			for (User curruser : listofUsers) {
				System.out.println(curruser);
				
			}
			listofUsers=bc.findMatchByMaxHeight(200);
			for (User user : listofUsers) {
				System.out.println(user);
				
			}
			listofUsers=bc.findMatchByBetweenHeight(140, 200);
			for (User curruser : listofUsers) {
				System.out.println(curruser);
				
			}
			
			listofUsers=bc.findMatchByMinWeight(88);
			for (User curruser : listofUsers) {
				System.out.println(curruser);
				
			}
			listofUsers=bc.findMatchByMax_Weight(40);
			for (User curruser : listofUsers) {
				System.out.println(curruser);
				
			}
			listofUsers=bc.findMatchByBetweenWeight(30, 100);
			for (User curruser : listofUsers) {
				System.out.println(curruser);
				
			}
			listofUsers=bc.findMatchByMinAge(40);
			for (User curruser : listofUsers) {
				System.out.println(curruser);
				
			}
			
			listofUsers=bc.findMatchByMax_Age(100);
            for (User curruser : listofUsers) {
        	System.out.println(curruser);
			
		    }
            listofUsers=bc.findMatchByBetweenAge(30, 100);
            for (User curruser : listofUsers) {
            	System.out.println(curruser);
				
			}
            listofUsers=bc.findMatchByMinSalary(10000);
            for (User curruser : listofUsers) {
            	System.out.println(curruser);
				
			}
            
            listofUsers=bc.findMatchByMax_Salary(100000);
            for (User curruser : listofUsers) {
        	   System.out.println(curruser);
			
		}
            listofUsers=bc.findMatchByBetweenSalary(10000, 100000);
            for (User curruser : listofUsers) {
            	System.out.println(curruser);
				
			}
	
	}
}

