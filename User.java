package com.abonda;

import java.util.regex.Matcher;

import java.util.regex.Pattern;

/**
 * This is implimentation of a class User It represents the users in a User
 * 
 * @author vchukka
 */
public class User {
	String name; // representing the name
	int age; // representing the age
	String profession; // representing the proffesion
	String primaryhobbie; // representing the primaryhobbie
	String Gender; // representing the gender
	String ph_no; // representing the phone no
	String martial_Status; // representing the martial status
	String location; // representing the location
	int salary; // representating the salary
	int Height; // representating the Height
	int Weight; // representating the Weight
	String Edu_qualification[] = new String[5]; // representating the Education qualification in ArrayofStrings
	String fav_foods[] = new String[5]; // representation the favourite food in ArrayofStrings
	String fav_places[] = new String[5]; // representating the favour places in ArrayofStrings
	String hobbies[] = new String[5]; // representing the hobbies in ArrayofStrings

	/**
	 * create a method with arguments and calling all the instance variables
	 * 
	 * @vchukka name
	 * @vchukka age
	 * @vchukka profession
	 * @vchukka primaryhobbie
	 * @vchukka gender
	 * @vchukka ph_no
	 * @vchukka martial_Status
	 * @vchukka location
	 * @vchukka salary
	 * @vchukka Height
	 * @vchukka Weight
	 * @vchukka degree
	 * @vchukka fav_foods
	 * @vchukka fav_places
	 * @vchukka hobbies
	 * @throws Exception
	 */
	// parameterized constructor
	public User(String name, int age, String profession, String primaryhobbie, String gender, String ph_no,
			String matrial_Status, String location, int salary, int Height, int Weight, String[] Edu_qualification,
			String fav_foods[], String fav_places[], String[] hobbies)  {
		// Height validation
//		if (Height < 140 && Height > 170) {
//			// validation of height failure
//			throw new Exception("invalid height");
//
//		} else if (age <= 18 && age >= 40) {
//			// validation of age failure
//			throw new Exception("invalid age");
//		} else if (Weight <= 40 && Weight >= 70) {
//			// validation of Weight failure
//			throw new Exception("invalid weight");
//		}
//
//		else {

			this.name = name;
			this.age = age;
			this.profession = profession;
			this.primaryhobbie = primaryhobbie;
			this.Gender = gender;
			this.ph_no = ph_no;
			this.martial_Status = martial_Status;
			this.location = location;
			this.salary = salary;
			this.Height = Height;
			this.Weight = Weight;
			this.fav_foods = fav_foods;
			this.fav_places = fav_places;
			this.Edu_qualification = Edu_qualification;
			this.hobbies = hobbies;
			System.out.println(this);

		}
	

	/**
	 * Overriding the toString method of user object in a human readable form.
	 */
	@Override
	public String toString() {
		String stringtoreturn = "This is user details below:\n";
		stringtoreturn += "name  		       	:     " + this.name + "\n";
		stringtoreturn += "age  		       	:     " + this.age + "\n";
		stringtoreturn += "profession  	       	:     " + this.profession + "\n";
		stringtoreturn += "primaryhobbie  	   	:     " + this.primaryhobbie + "\n";
		stringtoreturn += "Gender              : " + this.Gender + "\n";
		stringtoreturn += "ph_no  		       	:     " + this.ph_no + "\n";
		stringtoreturn += "martial_Status      : " + this.martial_Status + "\n";
		stringtoreturn += "location  		    :     " + this.location + "\n";
		stringtoreturn += "salary  		        :     " + this.salary + "\n";
		stringtoreturn += "Height  		        :     " + this.Height + "\n";
		stringtoreturn += "Weight	            :     " + this.Weight + "\n";

		System.out.println("------------------------");

		String hobbyString = "[";
		for (int i = 0; i < this.hobbies.length; i++) {
			hobbyString += "\"" + this.hobbies[i] + "\" " + " , ";
		}
		hobbyString += "]";
		stringtoreturn += "hobbies are       :" + hobbyString + "\n";

		String foodstring = "[";
		for (int i = 0; i < this.fav_foods.length; i++) {
			foodstring += "\" " + this.fav_foods[i] + "\" " + ",";
		}
		foodstring += "]";
		stringtoreturn += "Favourite Food: " + foodstring + "\n";

		String placestring = "[";
		for (int i = 0; i < this.fav_places.length; i++) {
			placestring += "\" " + this.fav_places[i] + "\" " + ",";
		}
		placestring += "]";
		stringtoreturn += "favourite palces: " + placestring + "\n";

		String eduQulstring = "[";
		for (int i = 0; i < this.Edu_qualification.length; i++) {
			eduQulstring += "\"" + this.Edu_qualification[i] + "\" " + ",";
		}
		eduQulstring += "]";
		stringtoreturn += "Education details are: " + eduQulstring + "\n";

		return stringtoreturn;

	}

	/**
	 * To generate a getters to get the value while during in searching of a string
	 * and Integer values.
	 * 
	 * @return
	 */
	public String getName() {
		return this.name;
	}

	public int getAge() {
		return this.age;
	}

	public String getProfession() {
		return this.profession;
	}

	public String getPrimaryhobbie() {
		return this.primaryhobbie;
	}

	public String getGender() {
		return this.Gender;
	}

	public String getPh_no() {
		return this.ph_no;
	}

	public String getMartial_Status() {
		return this.martial_Status;
	}

	public String getLocation() {
		return this.location;
	}

	public int getSalary() {
		return this.salary;
	}

	public int getHeight() {
		return this.Height;
	}

	public int getWeight() {
		return this.Weight;
	}

	public String getFoods() {

		String stringtoreturn = "[";

		for (int i = 0; i < fav_foods.length; i++) {
			stringtoreturn += fav_foods[i] + ((i != (fav_foods.length - 1)) ? "," : "");

		}
		return (stringtoreturn + "]");

	}

	public String getEdu_qualification() {
		String stringtoreturn = "[";
		for (int i = 0; i < Edu_qualification.length; i++) {
			stringtoreturn += Edu_qualification[i] + ((i != (Edu_qualification.length - 1)) ? "," : "");
		}
		return (stringtoreturn + "]");
	}

	public String getFav_places() {
		String stringtoreturn = "[";
		for (int i = 0; i < fav_places.length; i++) {
			stringtoreturn += fav_places[i] + ((i != (fav_places.length - 1)) ? "," : "");
		}
		return (stringtoreturn + "]");
	}

	public String getHobbies() {
		String stringtoreturn = "[";
		for (int i = 0; i < hobbies.length; i++) {
			stringtoreturn += hobbies[i] + ((i != (hobbies.length - 1)) ? "," : "");
		}
		return (stringtoreturn + "]");

	}

}
